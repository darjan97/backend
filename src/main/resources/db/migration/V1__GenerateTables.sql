CREATE TABLE IF NOT EXISTS "users" (
    id SERIAL PRIMARY KEY,
    email VARCHAR(254) NOT NULL UNIQUE,
    username VARCHAR(254) NOT NULL UNIQUE,
    password VARCHAR(300) NOT NULL,
    salt VARCHAR(1024),
    first_name VARCHAR(50),
    last_name VARCHAR(60),
    role VARCHAR(60),
    created TIMESTAMP DEFAULT now()
);

CREATE TABLE IF NOT EXISTS "posts" (
    id SERIAL PRIMARY KEY,
    author_id INT NOT NULL  REFERENCES users (id) ON DELETE CASCADE,
    title VARCHAR(254) NOT NULL,
    likes INT DEFAULT 0,
    description TEXT,
    type VARCHAR(200),
    created TIMESTAMP DEFAULT now()
);

CREATE TABLE IF NOT EXISTS "comments" (
    id SERIAL PRIMARY KEY,
    author_id INT NOT NULL REFERENCES users (id) ON DELETE CASCADE,
    post_id INT NOT NULL REFERENCES posts (id) ON DELETE CASCADE,
    content TEXT NOT NULL,
    created TIMESTAMP DEFAULT now()
);

CREATE TABLE IF NOT EXISTS "tags" (
    id SERIAL PRIMARY KEY,
    name VARCHAR(200),
    subsection VARCHAR(200)
);