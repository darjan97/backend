package com.students.shareknowledge.api;

import com.students.shareknowledge.exception.MyFileNotFoundException;
import com.students.shareknowledge.service.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLConnection;

@RestController
@RequestMapping("/files")
public class FileApi {
  @Autowired
  private FileStorageService fileStorageService;

  @GetMapping(path = "/{postId}/{fileName}")
  public void downloadFile(@PathVariable int postId, @PathVariable String fileName, HttpServletResponse response) {
    try {
      File file = fileStorageService.getFile(fileName, Integer.toString(postId));
      String mimeType = URLConnection.guessContentTypeFromName(file.getName());

      if (mimeType == null) {
        mimeType = "application/octet-stream";
      }

      response.setContentType(mimeType);
      response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + file.getName() + "\""));
      response.setContentLength((int) file.length());

      InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
      FileCopyUtils.copy(inputStream, response.getOutputStream());
    } catch (MyFileNotFoundException ex) {
      throw new ResponseStatusException(
          HttpStatus.NOT_FOUND,
          String.format("File %s not found", fileName),
          ex
      );
    } catch (Exception ex) {
      throw new ResponseStatusException(
          HttpStatus.INTERNAL_SERVER_ERROR,
          ex.getMessage(),
          ex
      );
    }
  }

  @DeleteMapping(path = "/{postId}/{fileName}")
  public void deleteFile(@PathVariable int postId, @PathVariable String fileName) {
    this.fileStorageService.delete(String.format("%s/%s", postId, fileName));
  }
}
