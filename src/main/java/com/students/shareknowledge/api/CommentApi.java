package com.students.shareknowledge.api;

import com.students.shareknowledge.dto.RequestCommentDto;
import com.students.shareknowledge.dto.ResponseCommentDto;
import com.students.shareknowledge.model.Comment;
import com.students.shareknowledge.model.User;
import com.students.shareknowledge.service.CommentService;
import com.students.shareknowledge.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/comments")
public class CommentApi {
  @Autowired
  private CommentService commentService;

  @Autowired
  private UserService userService;

  @PostMapping
  public ResponseEntity<ResponseCommentDto> createComment(@RequestBody RequestCommentDto reqDto) {
    try {
      Comment comment = this.commentService.createComment(reqDto.toModel());
      User author = this.userService.getUser(comment.getAuthorId());

      ResponseCommentDto respDto = new ResponseCommentDto(comment);
      respDto.setAuthorName(author.getUsername());
      respDto.setCreated(comment.getCreated());

      return new ResponseEntity<>(
          respDto,
          HttpStatus.OK
      );
    } catch (Exception ex) {
      throw new ResponseStatusException(
          HttpStatus.BAD_REQUEST,
          ex.getMessage(),
          ex
      );
    }
  }

  @PutMapping(path = "{id}")
  public ResponseEntity<Boolean> updateComment(@PathVariable("id") int id, @RequestBody RequestCommentDto dto) {
    try {
      this.commentService.updateComment(id, dto.toModel());

      return new ResponseEntity<>(
          true,
          HttpStatus.OK
      );
    } catch (Exception ex) {
      throw new ResponseStatusException(
          HttpStatus.BAD_REQUEST,
          ex.getMessage(),
          ex
      );
    }
  }

  @DeleteMapping(path = "{id}")
  public ResponseEntity<Boolean> deleteComment(@PathVariable("id") int id) {
    try {
      this.commentService.deleteComment(id);

      return new ResponseEntity<>(
          true,
          HttpStatus.OK
      );
    } catch (Exception ex) {
      throw new ResponseStatusException(
          HttpStatus.BAD_REQUEST,
          ex.getMessage(),
          ex
      );
    }
  }
}
