package com.students.shareknowledge.api;

import com.students.shareknowledge.dto.RequestUserDto;
import com.students.shareknowledge.dto.ResponseUserDto;
import com.students.shareknowledge.exception.UserDoesNotExistsException;
import com.students.shareknowledge.exception.UserException;
import com.students.shareknowledge.exception.UserExistsExceptin;
import com.students.shareknowledge.model.User;
import com.students.shareknowledge.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserApi {
  @Autowired
  private UserService userService;

  @GetMapping
  public ResponseEntity<List<ResponseUserDto>> getUsers() {
    return new ResponseEntity<>(
        this.convertUsersToDtos(this.userService.getAll()),
        HttpStatus.OK
    );
  }

  @GetMapping(path = "{id}")
  public ResponseEntity<ResponseUserDto> getUser(@PathVariable("id") int id) {
    try {
      return new ResponseEntity<>(
          new ResponseUserDto(this.userService.getUser(id)),
          HttpStatus.OK
      );
    } catch (UserDoesNotExistsException ex) {
      throw new ResponseStatusException(
          HttpStatus.BAD_REQUEST,
          "User with provided id doen't exiss",
          ex
      );
    } catch (Exception ex) {
      throw new ResponseStatusException(
          HttpStatus.INTERNAL_SERVER_ERROR,
          "Internal server error",
          ex
      );
    }

  }

  private List<ResponseUserDto> convertUsersToDtos(List<User> users) {
    List<ResponseUserDto> dtos = new ArrayList<>();

    for (int i = 0; i < users.size(); i++) {
      dtos.add(new ResponseUserDto(users.get(i)));
    }

    return dtos;
  }

  @PostMapping
  public ResponseEntity<ResponseUserDto> createUser(@RequestBody RequestUserDto requestUserDto) {
    try {
      User user = this.userService.createUser(requestUserDto.toModel());

      return new ResponseEntity<>(
          new ResponseUserDto(user),
          HttpStatus.OK
      );
    } catch (UserExistsExceptin ex) {
      throw new ResponseStatusException(
          HttpStatus.BAD_REQUEST,
          ex.getMessage(),
          ex
      );
    } catch (Exception ex) {
      throw new ResponseStatusException(
          HttpStatus.INTERNAL_SERVER_ERROR,
          ex.getMessage(),
          ex
      );
    }
  }

  @PutMapping(path = "{id}")
  public ResponseEntity<Boolean> updateUser(@PathVariable("id") int id, @RequestBody RequestUserDto requestUserDto) {
    try {
      this.userService.updateUser(id, requestUserDto.toModel());

      return new ResponseEntity<>(
          true,
          HttpStatus.OK
      );
    } catch (Exception ex) {
      throw new ResponseStatusException(
          HttpStatus.BAD_REQUEST,
          ex.getMessage(),
          ex
      );
    }
  }

  @DeleteMapping(path = "{id}")
  public ResponseEntity<Boolean> deleteUser(@PathVariable("id") int id) {
    try {
      this.userService.deleteUser(id);

      return new ResponseEntity<>(
          true,
          HttpStatus.OK
      );
    } catch (Exception ex) {
      throw new ResponseStatusException(
          HttpStatus.BAD_REQUEST,
          ex.getMessage(),
          ex
      );
    }
  }
}
