package com.students.shareknowledge.api;

import com.students.shareknowledge.dto.*;
import com.students.shareknowledge.exception.ItemNotDirectoryException;
import com.students.shareknowledge.exception.NonNullFieldsException;
import com.students.shareknowledge.exception.PostServiceException;
import com.students.shareknowledge.exception.UserDoesNotExistsException;
import com.students.shareknowledge.model.Comment;
import com.students.shareknowledge.model.Post;
import com.students.shareknowledge.model.User;
import com.students.shareknowledge.service.FileStorageService;
import com.students.shareknowledge.service.PostService;
import com.students.shareknowledge.service.UserService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/posts")
public class PostApi {
  @Autowired
  private PostService postService;

  @Autowired
  private UserService userService;

  @Autowired
  private FileStorageService fileStorageService;

  @GetMapping
  public ResponseEntity<List<ResponsePostDto>> getAllPost() {
    return new ResponseEntity<>(
        this.convertPostsToDtos(this.postService.getAllPosts()),
        HttpStatus.OK
    );
  }

  @NotNull
  private List<ResponsePostDto> convertPostsToDtos(@NotNull List<Post> posts) {
    List<ResponsePostDto> dtos = new ArrayList<>();

    for (int i = 0; i < posts.size(); i++) {
      dtos.add(this.convertToDto(posts.get(i)));
    }

    return dtos;
  }

  @GetMapping(path = "{id}")
  public ResponseEntity<ResponsePostDetailsDto> getPostDetails(@PathVariable("id") int id) {
    try {
      Post post = this.postService.getPost(id);
      List<Comment> comments = this.postService.getPostsComments(id);
      User author = this.userService.getUser(post.getAuthorId());

      ResponsePostDto postDto = this.convertToDto(post);
      ResponseUserDto authorDto = new ResponseUserDto(author);

      return new ResponseEntity<>(
          new ResponsePostDetailsDto(postDto, authorDto, convertCommentsToDtos(comments)),
          HttpStatus.OK
      );
    } catch (Exception ex) {
      throw new ResponseStatusException(
          HttpStatus.INTERNAL_SERVER_ERROR,
          "Internal server error",
          ex
      );
    }
  }

  @NotNull
  private ResponsePostDto convertToDto(Post post) {
    ResponsePostDto respDto = new ResponsePostDto(post);

    try {
      List<String> attachmentNames = this.fileStorageService.listDirFileNames(Integer.toString(post.getId()));
      respDto.setAttachmentNames(attachmentNames);
    } catch (ItemNotDirectoryException ex) {
      // Means that there are no attachments for provided post
    }

    return respDto;
  }

  @NotNull
  private List<ResponseCommentDto> convertCommentsToDtos(@NotNull List<Comment> comments) throws
      UserDoesNotExistsException {
    List<ResponseCommentDto> dtoList = new ArrayList<>();

    for (int i = 0; i < comments.size(); i++) {
      Comment comment = comments.get(i);
      ResponseCommentDto dto = new ResponseCommentDto(comment);

      try {
        User author = userService.getUser(comment.getAuthorId());
        dto.setAuthorName(author.getUsername());

        dtoList.add(dto);
      } catch (UserDoesNotExistsException ex) {
        throw ex;
      }
    }

    return dtoList;
  }

  @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
  public ResponseEntity<ResponsePostDto> createPost(@RequestParam("files") MultipartFile[] files, RequestPostDto postDto) {
    try {
      Post post = this.postService.createPost(postDto.toModel());
      ResponsePostDto responseDto = new ResponsePostDto(post);
      responseDto.setCreated(post.getCreated());

      if (files.length > 0) {
        List<String> attachmentNames = this.fileStorageService.storeFiles(files, Integer.toString(post.getId()));
        responseDto.setAttachmentNames(attachmentNames);
      }

      return new ResponseEntity<>(
          responseDto,
          HttpStatus.OK
      );
    } catch (NonNullFieldsException ex) {
      throw new ResponseStatusException(
          HttpStatus.BAD_REQUEST,
          ex.getMessage(),
          ex
      );
    } catch (Exception ex) {
      throw new ResponseStatusException(
          HttpStatus.INTERNAL_SERVER_ERROR,
          "Internal server error",
          ex
      );
    }
  }

  @PutMapping(path = "{id}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
  public ResponseEntity<Boolean> updatePost(@PathVariable("id") int id, @RequestParam("files") MultipartFile[] files, RequestPostDto post) {
    try {
      this.postService.updatePost(id, post.toModel());

      if (files.length > 0) {
        this.fileStorageService.storeFiles(files, Integer.toString(id));
      }

      return new ResponseEntity<>(
          true,
          HttpStatus.OK
      );
    } catch (PostServiceException ex) {
      throw new ResponseStatusException(
          HttpStatus.BAD_REQUEST,
          ex.getMessage(),
          ex
      );
    } catch (Exception ex) {
      throw new ResponseStatusException(
          HttpStatus.INTERNAL_SERVER_ERROR,
          "Internal server error",
          ex
      );
    }
  }

  @DeleteMapping(path = "{id}")
  public ResponseEntity<Boolean> deletePost(@PathVariable("id") int id) {
    try {
      this.postService.deletePost(id);
      this.fileStorageService.delete(Integer.toString(id));

      return new ResponseEntity<>(
          true,
          HttpStatus.OK
      );
    } catch (PostServiceException ex) {
      throw new ResponseStatusException(
          HttpStatus.BAD_REQUEST,
          ex.getMessage(),
          ex
      );
    } catch (Exception ex) {
      throw new ResponseStatusException(
          HttpStatus.INTERNAL_SERVER_ERROR,
          "Internal server error",
          ex
      );
    }
  }
}
