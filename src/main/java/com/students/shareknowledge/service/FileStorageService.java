package com.students.shareknowledge.service;

import com.students.shareknowledge.exception.FileStorageException;
import com.students.shareknowledge.exception.ItemNotDirectoryException;
import com.students.shareknowledge.exception.MyFileNotFoundException;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;

public interface FileStorageService {
  String storeFile(MultipartFile file, String directory) throws FileStorageException;
  List<String> storeFiles(MultipartFile[] files, String directory) throws FileStorageException;
  File getFile(String fileName, String directory) throws MyFileNotFoundException;
  List<File> getFiles(String directoryName) throws ItemNotDirectoryException;
  boolean delete(String filePath);
  List<String> listDirFileNames(String directoryName) throws ItemNotDirectoryException;
}
