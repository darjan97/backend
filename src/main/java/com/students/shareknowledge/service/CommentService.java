package com.students.shareknowledge.service;

import com.students.shareknowledge.exception.CommentServiceException;
import com.students.shareknowledge.model.Comment;

public interface CommentService {
  Comment createComment(Comment comment) throws CommentServiceException;
  void deleteComment(int id) throws CommentServiceException;
  void updateComment(int id, Comment comment) throws CommentServiceException;
}
