package com.students.shareknowledge.service;

import com.students.shareknowledge.dao.CommentDao;
import com.students.shareknowledge.dao.UserDao;
import com.students.shareknowledge.exception.CommentServiceException;
import com.students.shareknowledge.model.Comment;
import com.students.shareknowledge.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("commentService")
public class CommentServiceImpl implements CommentService {
  @Autowired
  private CommentDao commentDao;

  @Override
  public Comment createComment(Comment comment) throws CommentServiceException {
    try {
      return this.commentDao.save(comment);
    } catch (Exception ex) {
      throw new CommentServiceException("Couldn't create comment", ex);
    }
  }

  @Override
  public void deleteComment(int id) throws CommentServiceException {
    try {
      this.commentDao.delete(id);
    } catch (Exception ex) {
      throw new CommentServiceException("Couldn't delete comment", ex);
    }
  }

  @Override
  public void updateComment(int id, Comment comment) throws CommentServiceException {
    try {
      this.commentDao.update(id, comment);
    } catch (Exception ex) {
      throw new CommentServiceException("Couldn't update comment", ex);
    }
  }
}
