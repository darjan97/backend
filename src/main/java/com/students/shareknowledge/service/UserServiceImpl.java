package com.students.shareknowledge.service;

import com.students.shareknowledge.dao.UserDao;
import com.students.shareknowledge.exception.UserDoesNotExistsException;
import com.students.shareknowledge.exception.UserException;
import com.students.shareknowledge.exception.UserExistsExceptin;
import com.students.shareknowledge.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service("userService")
public class UserServiceImpl implements UserService {
  @Autowired
  private UserDao userDao;

  @Override
  public List<User> getAll() {
    return userDao.getAll();
  }

  @Override
  public User getUser(int id) throws UserDoesNotExistsException {
    try {
      return userDao.get(id);
    } catch (EmptyResultDataAccessException ex) {
      throw new UserDoesNotExistsException();
    }
  }

  @Override
  public User getUser(String usernameOrEmail) throws UserDoesNotExistsException {
    try {
      return userDao.get(usernameOrEmail);
    } catch (EmptyResultDataAccessException ex) {
      throw new UserDoesNotExistsException();
    }
  }

  @Override
  public User createUser(User user) throws UserExistsExceptin {
    try {
      return userDao.save(user);
    } catch (DuplicateKeyException ex) {
      throw new UserExistsExceptin();
    } catch (Exception ex) {
      throw ex;
    }
  }

  @Override
  public void deleteUser(int id) throws UserException {
    try {
      userDao.delete(id);
    } catch (SQLException ex) {
      throw new UserException("Something went wrong while deleting a user");
    }
  }

  @Override
  public void updateUser(int id, User user) throws UserException {
    try {
      userDao.update(id, user);
    } catch (SQLException ex) {
      throw new UserException("Something went wrong while updating a user");
    }
  }
}
