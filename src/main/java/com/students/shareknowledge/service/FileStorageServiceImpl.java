package com.students.shareknowledge.service;

import com.students.shareknowledge.config.FileStorageConfig;
import com.students.shareknowledge.exception.FileStorageException;
import com.students.shareknowledge.exception.ItemNotDirectoryException;
import com.students.shareknowledge.exception.MyFileNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service("fileStorageService")
public class FileStorageServiceImpl implements FileStorageService {
  private final Path fileStorageLocation;

  @Autowired
  public FileStorageServiceImpl(FileStorageConfig fileStorageConfig) throws FileStorageException {
    this.fileStorageLocation = Paths.get(fileStorageConfig.getStoragePath())
        .toAbsolutePath().normalize();

    try {
      Files.createDirectories(this.fileStorageLocation);
    } catch (Exception ex) {
      throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
    }
  }

  @Override
  public String storeFile(MultipartFile file, String directory) throws FileStorageException {
    String fileName = StringUtils.cleanPath(file.getOriginalFilename());

    try {
      if (fileName.contains("..")) {
        throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
      }

      Path postDirPath = Paths.get(String.format("%s/%s", fileStorageLocation.toString(), directory));

      if (!Files.exists(postDirPath)) {
        Files.createDirectories(postDirPath);
      }

      Path filePath = Paths.get(String.format("%s/%s", postDirPath.toString(), fileName));

      if (!Files.exists(filePath)) {
        Files.createFile(filePath);
      }

      Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
      return fileName;
    } catch (IOException ex) {
      throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
    }
  }

  @Override
  public List<String> storeFiles(MultipartFile[] files, String directory) throws FileStorageException {
    List<String> fileNames = new ArrayList<>();

    for (MultipartFile file : files) {
      fileNames.add(this.storeFile(file, directory));
    }

    return fileNames;
  }

  @Override
  public File getFile(String fileName, String directory) throws MyFileNotFoundException {
    File file = new File(this.getPath(String.format("%s/%s", directory, fileName)));

    if (!file.exists()) {
      throw new MyFileNotFoundException("File " + fileName + "does not exists!");
    }

    return file;
  }

  @Override
  public List<File> getFiles(String directoryName) throws ItemNotDirectoryException {
    File directory = new File(this.getPath(directoryName));

    if (!directory.isDirectory()) {
      throw new ItemNotDirectoryException("Item " + directoryName + "is not directory");
    }

    File[] files = directory.listFiles();

    if (files == null) {
      return Collections.emptyList();
    }

    return Stream.of(files)
        .filter(file -> !file.isDirectory())
        .collect(Collectors.toList());
  }

  @Override
  public boolean delete(String filePath) {
    File file = new File(this.getPath(filePath));

    if (!file.isDirectory()) {
      return file.delete();
    }

    for (File dirItem : file.listFiles()) {
      dirItem.delete();
    }

    return file.delete();
  }

  @Override
  public List<String> listDirFileNames(String directoryName) throws ItemNotDirectoryException {
    File directory = new File(this.getPath(directoryName));

    if (!directory.isDirectory()) {
      throw new ItemNotDirectoryException("Item " + directoryName + "is not directory");
    }

    File[] files = directory.listFiles();

    if (files == null) {
      return Collections.emptyList();
    }

    return Stream.of(files)
        .filter(file -> !file.isDirectory())
        .map(File::getName)
        .collect(Collectors.toList());
  }

  private String getPath(String relativePath) {
    return Paths.get(String.format("%s/%s", this.fileStorageLocation.toString(), relativePath))
        .normalize()
        .toString();
  }
}
