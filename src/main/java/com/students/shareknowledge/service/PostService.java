package com.students.shareknowledge.service;

import com.students.shareknowledge.exception.NonNullFieldsException;
import com.students.shareknowledge.exception.PostServiceException;
import com.students.shareknowledge.model.Comment;
import com.students.shareknowledge.model.Post;

import java.util.List;

public interface PostService {
  List<Post> getAllPosts();
  List<Comment> getPostsComments(int postId);
  Post getPost(int id) throws PostServiceException;
  Post createPost(Post post) throws NonNullFieldsException;
  void deletePost(int id) throws PostServiceException;
  void updatePost(int id, Post post) throws PostServiceException;

}
