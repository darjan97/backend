package com.students.shareknowledge.service;

import com.students.shareknowledge.dao.CommentDao;
import com.students.shareknowledge.dao.PostDao;
import com.students.shareknowledge.exception.NonNullFieldsException;
import com.students.shareknowledge.exception.PostServiceException;
import com.students.shareknowledge.model.Comment;
import com.students.shareknowledge.model.Post;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.xml.crypto.Data;
import java.sql.SQLException;
import java.util.List;

@Service("postService")
public class PostServiceImpl implements PostService {
  private final PostDao postDao;
  private final CommentDao commentDao;

  public PostServiceImpl(
      @Qualifier("PostDao") PostDao postDao,
      @Qualifier("CommentDao") CommentDao commentDao
    ) {
    this.postDao = postDao;
    this.commentDao = commentDao;
  }

  @Override
  public List<Post> getAllPosts() {
    return this.postDao.getAll();
  }

  @Override
  public List<Comment> getPostsComments(int postId) {
    return this.commentDao.getAll(postId);
  }

  @Override
  public Post getPost(int id) throws PostServiceException {
    try {
      return postDao.get(id);
    } catch (EmptyResultDataAccessException ex) {
      throw new PostServiceException("Post with provided id doesn't exists");
    }
  }

  @Override
  public Post createPost(Post post) throws NonNullFieldsException  {
    try {
      return this.postDao.save(post);
    } catch (DataIntegrityViolationException ex) {
      throw new NonNullFieldsException();
    } catch (Exception ex) {
      throw ex;
    }

  }

  @Override
  public void deletePost(int id) throws PostServiceException {
    try {
      this.postDao.delete(id);
    } catch (SQLException ex) {
      throw new PostServiceException("Post not delete, maybe ");
    }
  }

  @Override
  public void updatePost(int id, Post post) throws PostServiceException {
    try {
      this.postDao.update(id, post);
    } catch (SQLException ex) {
      throw new PostServiceException("Post updated");
    }
  }
}
