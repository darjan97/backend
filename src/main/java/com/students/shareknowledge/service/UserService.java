package com.students.shareknowledge.service;

import com.students.shareknowledge.exception.UserDoesNotExistsException;
import com.students.shareknowledge.exception.UserException;
import com.students.shareknowledge.exception.UserExistsExceptin;
import com.students.shareknowledge.model.User;

import java.util.List;

public interface UserService {
  List<User> getAll();
  User getUser(int id) throws UserDoesNotExistsException;
  User getUser(String usernameOrEmail) throws UserDoesNotExistsException;
  User createUser(User user) throws UserExistsExceptin;
  void deleteUser(int id) throws UserException;
  void updateUser(int id, User user) throws UserException;
}
