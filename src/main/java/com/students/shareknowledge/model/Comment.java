package com.students.shareknowledge.model;

import java.util.Date;

public class Comment {
  private int id;
  private int postId;
  private int authorId;
  private String content;
  private Date created;


  public Comment() {}

  public Comment(String content) {
    this.content = content;
  }

  public Comment(int id, String content) {
    this.id = id;
    this.content = content;
  }

  public Comment(int id, int postId, int authorId, String content, Date created) {
    this.id = id;
    this.postId = postId;
    this.content = content;
    this.authorId = authorId;
    this.created = created;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public int getPostId() {
    return postId;
  }

  public void setPostId(int postId) {
    this.postId = postId;
  }

  public int getAuthorId() {
    return authorId;
  }

  public void setAuthorId(int authorId) {
    this.authorId = authorId;
  }

  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }
}
