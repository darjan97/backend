package com.students.shareknowledge.model;

public class Tag {
  private int id;
  private String name;
  private String subsection;

  public Tag(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return this.getName();
  }
}
