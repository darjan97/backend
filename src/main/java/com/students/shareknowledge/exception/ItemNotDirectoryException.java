package com.students.shareknowledge.exception;

public class ItemNotDirectoryException extends RuntimeException {
  public ItemNotDirectoryException(String message) {
    super(message);
  }

  public ItemNotDirectoryException(String message, Throwable cause) {
    super(message, cause);
  }
}
