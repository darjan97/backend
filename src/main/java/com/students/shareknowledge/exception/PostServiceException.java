package com.students.shareknowledge.exception;

public class PostServiceException extends Exception {
  public PostServiceException(String message) {
    super(message);
  }

  public PostServiceException(String message, Throwable cause) {
    super(message, cause);
  }
}
