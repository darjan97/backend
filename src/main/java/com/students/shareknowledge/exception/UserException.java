package com.students.shareknowledge.exception;

public class UserException extends Exception {
  public UserException() {
  }

  public UserException(String message) {
    super(message);
  }
}
