package com.students.shareknowledge.exception;

public class UserDoesNotExistsException extends Exception {
  public UserDoesNotExistsException() {
  }

  public UserDoesNotExistsException(String message) {
    super(message);
  }
}
