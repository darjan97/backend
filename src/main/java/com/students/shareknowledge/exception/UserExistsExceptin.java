package com.students.shareknowledge.exception;

public class UserExistsExceptin extends Exception {
  public UserExistsExceptin() {
    super("User with provided email/username already exists");
  }
}
