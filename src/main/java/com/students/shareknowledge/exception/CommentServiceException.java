package com.students.shareknowledge.exception;

public class CommentServiceException extends Exception {
  public CommentServiceException(String message) {
    super(message);
  }

  public CommentServiceException(String message, Throwable cause) {
    super(message, cause);
  }
}
