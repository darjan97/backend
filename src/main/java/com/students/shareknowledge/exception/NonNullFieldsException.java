package com.students.shareknowledge.exception;

public class NonNullFieldsException extends Exception {
  public NonNullFieldsException() {
    super("Non null fields are null");
  }
}
