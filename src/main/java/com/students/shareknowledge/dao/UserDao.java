package com.students.shareknowledge.dao;

import com.students.shareknowledge.model.User;
import org.springframework.dao.EmptyResultDataAccessException;

import java.sql.SQLException;
import java.util.List;

public interface UserDao {
  List<User> getAll();
  User get(int id) throws EmptyResultDataAccessException;
  User get(String usernameOrEmail) throws EmptyResultDataAccessException;
  User save(User user);
  void delete(int id) throws SQLException;
  void update(int id, User user) throws SQLException;
}
