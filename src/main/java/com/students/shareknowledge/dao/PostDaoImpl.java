package com.students.shareknowledge.dao;

import com.students.shareknowledge.mapper.PostMapper;
import com.students.shareknowledge.model.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.Date;
import java.util.List;

@Component("PostDao")
public class PostDaoImpl implements PostDao {
  private final JdbcTemplate jdbcTemplate;

  @Autowired
  public PostDaoImpl(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  @Override
  public List<Post> getAll() {
    String sql = "SELECT * FROM \"posts\";";
    return jdbcTemplate.query(sql, new PostMapper());
  }

  @Override
  public List<Post> getAll(int userId) {
    String sql = "SELECT * FROM \"posts\" WHERE author_id = ?;";
    return jdbcTemplate.query(sql, new Object[]{userId}, new PostMapper());
  }

  @Override
  public Post get(int id) throws EmptyResultDataAccessException {
    String sql = "SELECT * FROM \"posts\" WHERE id = ?;";
    return jdbcTemplate.queryForObject(sql, new Object[]{id}, new PostMapper());
  }

  @Override
  public Post save(final Post post) throws DataIntegrityViolationException {
    String SQL = "INSERT INTO \"posts\" (author_id, title, description, type)"
        + "VALUES (?, ?, ?, ?);";

    KeyHolder holder = new GeneratedKeyHolder();

    int result = jdbcTemplate.update((Connection connection) -> {
      PreparedStatement ps = connection.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);

      ps.setInt(1, post.getAuthorId());
      ps.setString(2, post.getTitle());
      ps.setString(3, post.getDescription());
      ps.setString(4, post.getType());

      return ps;
    }, holder);

    post.setId((int) holder.getKeys().get("id"));
    post.setCreated((Date) holder.getKeys().get("created"));

    return post;
  }

  @Override
  public void delete(int id) throws SQLException {
    String sql = "DELETE FROM \"posts\" WHERE id = ?;";

    int result = jdbcTemplate.update(sql, id);

    if (result != 1) {
      throw new SQLException("Something went wrong while deleting post from db");
    }
  }

  @Override
  public void update(int id, Post post) throws SQLException {
    String sql = "UPDATE \"posts\"" +
        "SET title = ?, description = ?, type = ? " +
        "WHERE id = ?;";

    final Object[] params = new Object[]{
        post.getTitle(),
        post.getDescription(),
        post.getType(),
        id
    };

    final int[] types = {
        Types.VARCHAR,
        Types.VARCHAR,
        Types.VARCHAR,
        Types.INTEGER
    };

    int result = jdbcTemplate.update(sql, params, types);

    if (result != 1) {
      throw new SQLException("Something went wrong while updating post in db");
    }
  }
}
