package com.students.shareknowledge.dao;

import com.students.shareknowledge.model.Post;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;

import java.sql.SQLException;
import java.util.List;

public interface PostDao {
  List<Post> getAll();
  List<Post> getAll(int userId);
  Post get(int id) throws EmptyResultDataAccessException;
  Post save(Post post) throws DataIntegrityViolationException;
  void delete(int id) throws SQLException;
  void update(int id, Post post) throws SQLException;
}
