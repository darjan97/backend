package com.students.shareknowledge.dao;

import com.students.shareknowledge.model.Comment;
import org.springframework.dao.EmptyResultDataAccessException;

import java.sql.SQLException;
import java.util.List;

public interface CommentDao {
  List<Comment> getAll();
  List<Comment> getAll(int postId);
  Comment get(int id) throws EmptyResultDataAccessException;
  Comment save(Comment comment) throws SQLException;
  void delete(int id) throws SQLException;
  void update(int id, Comment comment) throws SQLException;
}
