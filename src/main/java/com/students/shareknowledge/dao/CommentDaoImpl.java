package com.students.shareknowledge.dao;

import com.students.shareknowledge.mapper.CommentMapper;
import com.students.shareknowledge.model.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.List;
import java.util.Date;

@Component("CommentDao")
public class CommentDaoImpl implements CommentDao {
  private final JdbcTemplate jdbcTemplate;

  @Autowired
  public CommentDaoImpl(JdbcTemplate jdbcTemplate) { this.jdbcTemplate = jdbcTemplate; }

  @Override
  public List<Comment> getAll(int postId) {
    String sql = "SELECT * FROM \"comments\" WHERE post_id = ?;";
    return jdbcTemplate.query(sql, new Object[]{postId}, new CommentMapper());
  }

  @Override
  public List<Comment> getAll() {
    String sql = "SELECT * FROM \"comments\";";
    return jdbcTemplate.query(sql, new CommentMapper());
  }

  @Override
  public Comment get(int id) throws EmptyResultDataAccessException {
    String sql = "SELECT * FROM \"comments\" WHERE id = ?;";
    return jdbcTemplate.queryForObject(sql, new Object[]{id}, new CommentMapper());
  }

  @Override
    public Comment save(final Comment comment) throws SQLException {
    final String SQL = "INSERT INTO \"comments\" (post_id, author_id, content)"
     + "VALUES (?, ?, ?);";

    KeyHolder holder = new GeneratedKeyHolder();

    try {
      jdbcTemplate.update((Connection connection) -> {
        PreparedStatement ps = connection.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);

        ps.setInt(1, comment.getPostId());
        ps.setInt(2, comment.getAuthorId());
        ps.setString(3, comment.getContent());

        return ps;
      }, holder);

      comment.setId((int) holder.getKeys().get("id"));
      comment.setCreated((Date) holder.getKeys().get("created"));

      return comment;
    } catch (Exception ex) {
      throw new SQLException("Something went wrong while saving comment in db", ex);
    }
  }

  @Override
  public void delete(int id) throws SQLException {
    String sql = "DELETE FROM \"comments\" WHERE id = ?;";

    int result = jdbcTemplate.update(sql, id);

    if (result != 1) {
      throw new SQLException("Something went wrong while deleting comment from db");
    }
  }

  @Override
  public void update(int id, Comment comment) throws SQLException {
    String sql = "UPDATE \"comments\"" +
        "SET content = ? " +
        "WHERE id = ?;";

    final Object[] params = new Object[] {
        comment.getContent(),
        id
    };

    final int[] types = {
      Types.VARCHAR,
      Types.INTEGER
    };

    int result = jdbcTemplate.update(sql, params, types);

    if (result != 1) {
      throw new SQLException("Something went wrong while updating comment in db");
    }
  }
}
