package com.students.shareknowledge.dao;

import com.students.shareknowledge.mapper.UserMapper;
import com.students.shareknowledge.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.List;

@Component("UserDao")
public class UserDaoImpl implements UserDao {
  private final JdbcTemplate jdbcTemplate;

  @Autowired
  public UserDaoImpl(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  @Override
  public List<User> getAll() {
    String sql = "SELECT * FROM \"users\";";
    return this.jdbcTemplate.query(sql, new UserMapper());
  }

  @Override
  public User get(int id) throws EmptyResultDataAccessException {
    String sql = "SELECT * FROM \"users\" WHERE id = ?;";
    return jdbcTemplate.queryForObject(sql, new Object[]{id}, new UserMapper());
  }

  @Override
  public User get(String usernameOrEmail) throws EmptyResultDataAccessException {
    String sql = "SELECT * FROM \"users\" WHERE username = ? OR email = ?;";
    return jdbcTemplate.queryForObject(sql, new Object[]{usernameOrEmail, usernameOrEmail}, new UserMapper());
  }

  @Override
  public User save(User user) {
    String SQL = "INSERT INTO \"users\" (email, username, password, first_name, last_name)"
        + "VALUES (?, ?, ?, ?, ?);";

    KeyHolder holder = new GeneratedKeyHolder();

    int result = jdbcTemplate.update((Connection connection) -> {
      PreparedStatement ps = connection.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);

      ps.setString(1, user.getEmail());
      ps.setString(2, user.getUsername());
      ps.setString(3, user.getPassword());
      ps.setString(4, user.getFirstName());
      ps.setString(5, user.getLastName());

      return ps;
    }, holder);

    user.setId((int) holder.getKeys().get("id"));
    return user;
  }

  @Override
  public void delete(int id) throws SQLException {
    String sql = "DELETE FROM \"users\" WHERE id = ?;";

    int result = jdbcTemplate.update(sql, id);

    if (result != 1) {
      throw new SQLException("Something went wrong while deleting user from db");
    }
  }

  @Override
  public void update(int id, User user) throws SQLException {
    String sql = "UPDATE \"users\"" +
        "SET username = ?, password = ?, fist_name = ?, first_name = ? " +
        "WHERE id = ?;";

    final Object[] params = new Object[]{
        user.getUsername(),
        user.getPassword(),
        user.getFirstName(),
        user.getLastName(),
        id
    };

    final int[] types = {
        Types.VARCHAR,
        Types.VARCHAR,
        Types.VARCHAR,
        Types.VARCHAR,
        Types.INTEGER
    };

    int result = jdbcTemplate.update(sql, params, types);

    if (result != 1) {
      throw new SQLException("Something went wrong while updating user in db");
    }
  }
}
