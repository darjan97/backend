package com.students.shareknowledge.mapper;

import com.students.shareknowledge.model.Post;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PostMapper implements RowMapper<Post> {
  @Override
  public Post mapRow(ResultSet resultSet, int i) throws SQLException {
    Post post = new Post();

    post.setId(resultSet.getInt("id"));
    post.setAuthorId(resultSet.getInt("author_id"));
    post.setTitle(resultSet.getString("title"));
    post.setDescription(resultSet.getString("description"));
    post.setType(resultSet.getString("type"));
    post.setCreated(resultSet.getTimestamp("created"));

    return post;
  }
}
