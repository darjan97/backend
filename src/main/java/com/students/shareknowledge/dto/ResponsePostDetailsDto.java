package com.students.shareknowledge.dto;

import java.util.ArrayList;
import java.util.List;

public class ResponsePostDetailsDto {
  private ResponsePostDto post;
  private ResponseUserDto author;
  private List<ResponseCommentDto> comments;

  public ResponsePostDetailsDto(ResponsePostDto post, ResponseUserDto author) {
    this.post = post;
    this.author = author;
    this.comments = new ArrayList<>();
  }

  public ResponsePostDetailsDto(ResponsePostDto post, ResponseUserDto author, List<ResponseCommentDto> comments) {
    this.post = post;
    this.author = author;
    this.comments = comments;
  }

  public ResponsePostDto getPost() {
    return post;
  }

  public void setPost(ResponsePostDto post) {
    this.post = post;
  }

  public List<ResponseCommentDto> getComments() {
    return comments;
  }

  public void setComments(List<ResponseCommentDto> postComments) {
    this.comments = postComments;
  }

  public ResponseUserDto getAuthor() {
    return author;
  }

  public void setAuthor(ResponseUserDto author) {
    this.author = author;
  }
}
