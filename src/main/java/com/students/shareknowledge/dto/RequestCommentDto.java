package com.students.shareknowledge.dto;

import com.students.shareknowledge.model.Comment;

public class RequestCommentDto {
  private int authorId;
  private int postId;
  private String content;

  public RequestCommentDto() {}

  public RequestCommentDto(Comment comment) {
    this.authorId = comment.getAuthorId();
    this.postId = comment.getPostId();
    this.content = comment.getContent();
  }

  public Comment toModel() {
    Comment comment = new Comment();

    comment.setContent(this.content);
    comment.setAuthorId(this.authorId);
    comment.setPostId(this.postId);

    return comment;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public int getAuthorId() {
    return authorId;
  }

  public void setAuthorId(int authorId) {
    this.authorId = authorId;
  }

  public int getPostId() {
    return postId;
  }

  public void setPostId(int postId) {
    this.postId = postId;
  }
}
