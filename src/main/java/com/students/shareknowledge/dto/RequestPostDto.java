package com.students.shareknowledge.dto;

import com.students.shareknowledge.model.Post;

public class RequestPostDto {
  private int authorId;
  private String title;
  private String description;
  private String type;

  public void loadValues(Post post) {
    this.setAuthorId(post.getAuthorId());
    this.setDescription(post.getDescription());
    this.setTitle(post.getTitle());
    this.setType(post.getType());
  }

  public Post toModel () {
    Post post = new Post();

    post.setAuthorId(this.getAuthorId());
    post.setDescription(this.getDescription());
    post.setTitle(this.getTitle());
    post.setType(this.getType());

    return post;
  }

  public int getAuthorId() {
    return authorId;
  }

  public void setAuthorId(int authorId) {
    this.authorId = authorId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }
}
