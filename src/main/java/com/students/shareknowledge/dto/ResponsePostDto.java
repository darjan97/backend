package com.students.shareknowledge.dto;

import com.students.shareknowledge.model.Post;

import java.util.Date;
import java.util.List;

public class ResponsePostDto {
  private int id;
  private int authorId;
  private String title;
  private String description;
  private String type;
  private List<String> attachmentNames;
  private int likes;
  private Date created;

  public ResponsePostDto(Post post) {
    this.setId(post.getId());
    this.setAuthorId(post.getAuthorId());
    this.setDescription(post.getDescription());
    this.setTitle(post.getTitle());
    this.setType(post.getType());
    this.setLikes(post.getLikes());
    this.setCreated(post.getCreated());
    this.attachmentNames = null;
  }

  public Post toModel () {
    Post post = new Post();

    post.setId(this.getId());
    post.setAuthorId(this.getAuthorId());
    post.setDescription(this.getDescription());
    post.setTitle(this.getTitle());
    post.setType(this.getType());
    post.setLikes(this.getLikes());
    post.setCreated(this.getCreated());

    return post;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getAuthorId() {
    return authorId;
  }

  public void setAuthorId(int authorId) {
    this.authorId = authorId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public int getLikes() {
    return likes;
  }

  public void setLikes(int likes) {
    this.likes = likes;
  }

  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  public List<String> getAttachmentNames() {
    return attachmentNames;
  }

  public void setAttachmentNames(List<String> attachmentNames) {
    this.attachmentNames = attachmentNames;
  }
}
