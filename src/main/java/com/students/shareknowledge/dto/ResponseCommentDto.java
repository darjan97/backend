package com.students.shareknowledge.dto;

import com.students.shareknowledge.model.Comment;

import java.util.Date;

public class ResponseCommentDto {
  private int id;
  private int authorId;
  private int postId;
  private String content;
  private String authorName;
  private Date created;

  public ResponseCommentDto() {}

  public ResponseCommentDto(Comment comment) {
    this.id = comment.getId();
    this.authorId = comment.getAuthorId();
    this.postId = comment.getPostId();
    this.content = comment.getContent();
    this.created = comment.getCreated();
  }

  public Comment toModel() {
    return new Comment(this.id, this.postId, this.authorId, this.content, this.created);
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String description) {
    this.content = description;
  }

  public String getAuthorName() {
    return authorName;
  }

  public void setAuthorName(String authorName) {
    this.authorName = authorName;
  }

  public int getAuthorId() {
    return authorId;
  }

  public void setAuthorId(int authorId) {
    this.authorId = authorId;
  }

  public int getPostId() {
    return postId;
  }

  public void setPostId(int postId) {
    this.postId = postId;
  }

  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }
}
