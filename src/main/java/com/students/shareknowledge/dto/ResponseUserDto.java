package com.students.shareknowledge.dto;

import com.students.shareknowledge.model.User;

public class ResponseUserDto {
  private int id;
  private String email;
  private String username;
  private String firstName;
  private String lastName;

  public ResponseUserDto(User user) {
    this.id = user.getId();
    this.email = user.getEmail();
    this.username = user.getUsername();
    this.firstName = user.getFirstName();
    this.lastName = user.getLastName();
  }

  public User toModel() {
    User user = new User();

    user.setId(this.id);
    user.setEmail(this.email);
    user.setUsername(this.username);
    user.setFirstName(this.firstName);
    user.setLastName(this.lastName);

    return user;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }
}
