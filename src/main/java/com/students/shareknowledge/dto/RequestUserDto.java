package com.students.shareknowledge.dto;

import com.students.shareknowledge.model.User;

public class RequestUserDto {
  private String email;
  private String username;
  private String password;
  private String firstName;
  private String lastName;

  public RequestUserDto(String email, String username) {
    this.email = email;
    this.username = username;
  }

  public User toModel() {
    User user = new User();

    user.setEmail(email);
    user.setUsername(username);
    user.setPassword(password);
    user.setFirstName(firstName);
    user.setLastName(lastName);

    return user;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
}
