package com.students.shareknowledge;

import com.students.shareknowledge.config.FileStorageConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({
		FileStorageConfig.class
})
public class ShareknowledgeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShareknowledgeApplication.class, args);
	}

}
